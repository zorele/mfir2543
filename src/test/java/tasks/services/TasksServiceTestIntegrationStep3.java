package tasks.services;

import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import tasks.model.Task;
import tasks.model.ArrayTaskList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class TasksServiceTestIntegrationStep3 {
    ArrayTaskList arrayTaskList;
    TasksService tasksService;

    @BeforeEach
    void setUp() {
        arrayTaskList = new ArrayTaskList();
        tasksService = new TasksService(arrayTaskList);
    }

    @Test
    void getObservableList() throws ParseException {
        // arrange
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date date = formatter.parse("2021-03-21 16:00");
        Task task1 = new Task("Task1", date);

        date = formatter.parse("2020-03-21 16:00");
        Task task2 = new Task("Task2", date);

        date = formatter.parse("2021-05-28 19:00");
        Task task3 = new Task("Task3", date);

        // act
        arrayTaskList.add(task1);
        arrayTaskList.add(task2);
        arrayTaskList.add(task3);
        ObservableList<Task> observableList = tasksService.getObservableList();

        // assert
        assertEquals(observableList.size() , 3);
        assertEquals(observableList.get(0).getTitle(), "Task1");
        assertEquals(observableList.get(1).getTitle(), "Task2");
        assertEquals(observableList.get(2).getTitle(), "Task3");
    }

    @Test
    void filterTasks() throws Exception {
        // arrange
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date date = formatter.parse("2021-03-21 16:00");
        Task task1 = new Task("Task1", date);
        task1.setActive(true);

        date = formatter.parse("2020-03-21 16:00");
        Task task2 = new Task("Task2", date);
        task2.setActive(true);

        date = formatter.parse("2021-05-28 19:00");
        Task task3 = new Task("Task3", date);
        task3.setActive(true);

        // act
        arrayTaskList.add(task1);
        arrayTaskList.add(task2);
        arrayTaskList.add(task3);

        LocalDate dataStart = LocalDate.of(2021,2,21);
        LocalDate dataEnd = LocalDate.of(2021,5,20);
        String oraStart = "01:00";
        String oraEnd = "23:57";

        DateService dateService = new DateService(tasksService);

        Iterable<Task> filtredTasks = tasksService.filterTasks(dataStart, dataEnd, oraStart, oraEnd, dateService);

        ArrayList<Task> output = new ArrayList<>();
        output.add(task1);

        // assert
        assertEquals(filtredTasks, output);
    }
}