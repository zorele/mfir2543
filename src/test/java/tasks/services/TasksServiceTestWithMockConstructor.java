package tasks.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import tasks.model.ArrayTaskList;
import tasks.model.Task;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

class TasksServiceTestWithMockConstructor {
    ArrayTaskList arrayTaskList;
    TasksService tasksService;
    Task task;

    @BeforeEach
    void setUp() {
        arrayTaskList = mock(ArrayTaskList.class);
        tasksService = new TasksService(arrayTaskList);

    }

    @Test
    void formTimeUnit() {
        assertEquals(tasksService.formTimeUnit(30), "30");
    }

    @Test
    void parseFromStringToSeconds() {
        Exception exception = assertThrows(Exception.class, () -> {
            tasksService.parseFromStringToSeconds("1234");
        });
    }
}