package tasks.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class TaskTest {

    private Task task1, task2;

    @BeforeEach
    void setUp() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date date = formatter.parse("2021-03-21 16:00");
        this.task1 = new Task("task1", date);
        this.task1.setActive(true);

        date = formatter.parse("2021-03-21 16:00");
        Date date2 = formatter.parse("2021-07-21 20:00");
        this.task2 = new Task("task2", date, date2, 86400);
        this.task2.setActive(true);
    }

    @Test
    void isActive() {
        boolean activ = task1.isActive();
        assertTrue(activ);
    }

    @Test
    void isRepeated() {
        boolean repeated = task2.isRepeated();
        assertTrue(repeated);
    }
}