package tasks.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import tasks.services.DateService;
import tasks.services.TasksService;
import tasks.services.ValidationException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

class TasksOperationsTest {

    private TasksOperations tasksOperations;
    private DateService dateService;
    private TasksService tasksService;
    private LocalDate dataStart = LocalDate.now();
    private LocalDate dataEnd = LocalDate.now();
    private String oraStart = "00:59";
    private String oraEnd = "13:00";
    private ArrayTaskList listTasks0 = new ArrayTaskList();
    private Task task1, task2, task3, task4, task5;

    @BeforeEach
    void setUp() throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        Date date = formatter.parse("2021-03-21 16:00");
        this.task1 = new Task("task1", date);
        this.task1.setActive(true);
        date = formatter.parse("2020-03-21 16:00");
        this.task2 = new Task("task2", date);
        this.task2.setActive(true);
        date = formatter.parse("2021-03-21 16:00");
        Date date2 = formatter.parse("2021-07-21 20:00");
        this.task3 = new Task("task3", date, date2, 86400);
        this.task3.setActive(true);
        date = formatter.parse("2021-05-28 19:00");
        this.task4 = new Task("task4", date);
        this.task4.setActive(true);
        this.task5 = new Task("task5", date);
        this.task5.setActive(true);

        this.tasksService = new TasksService(listTasks0);
        this.dateService = new DateService(tasksService);
        ObservableList<Task> tasks = FXCollections.observableArrayList();
        tasks.add(task1);
        this.tasksOperations = new TasksOperations(tasks);
    }

    @AfterEach
    void tearDown() {
    }

    @Tag("Testare minute oraStart")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds1(){
        this.oraEnd = "10:00";
        this.oraStart = "00:60";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("minute invalide"));
    }

    @Tag("Testare minute oraStart")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds2(){
        this.oraEnd = "10:00";
        this.oraStart = "00:-9";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("minute invalide"));
    }

    @Tag("Testare ora oraStart")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds3(){
        this.oraEnd = "10:00";
        this.oraStart = "24:00";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("ora invalida"));
    }

    @Tag("Testare ora oraStart")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds4(){
        this.oraEnd = "10:00";
        this.oraStart = "-3:00";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("ora invalida"));
    }

    @Tag("Testare ora oraEnd")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds5(){
        this.oraEnd = "25:10";
        this.oraStart = "13:00";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("ora invalida"));
    }

    @Tag("Testare ora oraEnd")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds6(){
        this.oraEnd = "-7:10";
        this.oraStart = "13:00";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("ora invalida"));
    }

    @Tag("Testare minute oraEnd")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds7(){
        this.oraEnd = "09:74";
        this.oraStart = "13:00";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("minute invalide"));
    }

    @Tag("Testare minute oraEnd")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds8(){
        this.oraEnd = "09:-2";
        this.oraStart = "13:00";
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("minute invalide"));
    }

    @Tag("Testare dataStart dupa dataEnd")
    @Test
    void whenExceptionThrown_thenAssertionSucceeds9(){
        this.oraEnd = "09:00";
        this.oraStart = "13:00";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        this.dataStart = LocalDate.of(2021, 4, 2);
        this.dataEnd = LocalDate.of(2021,2,1);
        ValidationException exception = assertThrows(ValidationException.class, () -> {
            tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
        });
        String mesaj = exception.getMessage();
        assertTrue(mesaj.contains("ore invalide"));
    }

    @Tag("Testare returnare []")
    @Test
    void whenEmptyList(){
        try{
            this.dataStart = LocalDate.of(2021,4,21);
            this.dataEnd = LocalDate.of(2021,5,20);
            this.oraStart = "01:00";
            this.oraEnd = "23:57";
            ObservableList<Task> tasks = FXCollections.observableArrayList();
            this.tasksOperations = new TasksOperations(tasks);
            Iterable<Task> filtredTasks = tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
            Iterable<Task> output = new ArrayList<>();
            assertEquals(filtredTasks, output);
        }
        catch (ValidationException e){
            fail("Eroare validare");
        }
    }

    @Test
    void testFiltare1(){
        try{
            this.dataStart = LocalDate.of(2021,2,21);
            this.dataEnd = LocalDate.of(2021,5,20);
            this.oraStart = "01:00";
            this.oraEnd = "23:57";
            ObservableList<Task> tasks = FXCollections.observableArrayList();
            tasks.add(task1);
            this.tasksOperations = new TasksOperations(tasks);
            Iterable<Task> filtredTasks = tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
            ArrayList<Task> output = new ArrayList<>();
            output.add(task1);
            assertEquals(filtredTasks, output);
        }
        catch (ValidationException e){
            fail("Eroare validare");
        }
    }

    @Test
    void testFiltrare2(){
        try{
            this.dataStart = LocalDate.of(2021,2,21);
            this.dataEnd = LocalDate.of(2021,5,20);
            this.oraStart = "01:00";
            this.oraEnd = "23:57";
            ObservableList<Task> tasks = FXCollections.observableArrayList();
            tasks.add(task2);
            this.tasksOperations = new TasksOperations(tasks);
            Iterable<Task> filtredTasks = tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
            Iterable<Task> output = new ArrayList<>();
            assertEquals(filtredTasks, output);
        }
        catch (ValidationException e){
            fail("Eroare validare");
        }
    }

    @Test
    void testFiltrare3(){
        try{
            this.dataStart = LocalDate.of(2021,2,21);
            this.dataEnd = LocalDate.of(2021,5,20);
            this.oraStart = "01:00";
            this.oraEnd = "23:57";
            ObservableList<Task> tasks = FXCollections.observableArrayList();
            tasks.add(task1);
            tasks.add(task2);
            this.tasksOperations = new TasksOperations(tasks);
            Iterable<Task> filtredTasks = tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
            ArrayList<Task> output = new ArrayList<>();
            output.add(task1);
            assertEquals(filtredTasks, output);
        }
        catch (ValidationException e){
            fail("Eroare validare");
        }
    }

    @Test
    void testFiltrare4(){
        try{
            this.dataStart = LocalDate.of(2021,3,21);
            this.dataEnd = LocalDate.of(2021,3,22);
            this.oraStart = "17:00";
            this.oraEnd = "16:00";
            ObservableList<Task> tasks = FXCollections.observableArrayList();
            tasks.add(task3);
            this.tasksOperations = new TasksOperations(tasks);
            Iterable<Task> filtredTasks = tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
            ArrayList<Task> output = new ArrayList<>();
            output.add(task3);
            assertEquals(filtredTasks, output);
        }
        catch (ValidationException e){
            fail("Eroare validare");
        }
    }

    @Test
    void testFiltrare5(){
        try{
            this.dataStart = LocalDate.of(2021,2,21);
            this.dataEnd = LocalDate.of(2021,9,21);
            this.oraStart = "17:00";
            this.oraEnd = "16:00";
            ObservableList<Task> tasks = FXCollections.observableArrayList();
            tasks.add(task3);
            tasks.add(task4);
            tasks.add(task5);
            this.tasksOperations = new TasksOperations(tasks);
            Iterable<Task> filtredTasks = tasksOperations.incoming(this.dataStart, this.dataEnd, this.oraStart, this.oraEnd, this.dateService);
            ArrayList<Task> output = new ArrayList<>();
            output.add(task3);
            output.add(task4);
            output.add(task5);
            assertEquals(filtredTasks, output);
        }
        catch (ValidationException e){
            fail("Eroare validare");
        }
    }
}
