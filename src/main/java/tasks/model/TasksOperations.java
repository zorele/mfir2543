package tasks.model;

import javafx.collections.ObservableList;
import tasks.services.DateService;
import tasks.services.ValidationException;

import java.time.LocalDate;
import java.util.*;

public class TasksOperations {

    public ArrayList<Task> tasks;

    public TasksOperations(ObservableList<Task> tasksList){
        tasks = new ArrayList<>();
        tasks.addAll(tasksList);
    }

    public Iterable<Task> incoming(LocalDate dataStart, LocalDate dataEnd, String oraStart, String oraEnd, DateService dateService){

        String[] unitStart = oraStart.split(":");
        int hours = Integer.parseInt(unitStart[0]);
        int minutes = Integer.parseInt(unitStart[1]);

        if (hours < 0 || hours > 23)
            throw new ValidationException("ora invalida");

        if (minutes < 0 || minutes > 59)
            throw new ValidationException("minute invalide");

        String[] unitEnd = oraEnd.split(":");
        hours = Integer.parseInt(unitEnd[0]);
        minutes = Integer.parseInt(unitEnd[1]);

        if (hours < 0 || hours > 23)
            throw new ValidationException("ora invalida");

        if (minutes < 0 || minutes > 59)
            throw new ValidationException("minute invalide");

        Date start = getDateFromFilterField(dataStart, oraStart, dateService);
        Date end = getDateFromFilterField(dataEnd, oraEnd, dateService);

        System.out.println(start);
        System.out.println(end);

        if (start.after(end))
            throw new ValidationException("ore invalide");

        ArrayList<Task> incomingTasks = new ArrayList<>();
        for (Task t : tasks) {
            Date nextTime = t.nextTimeAfter(start);
            if (nextTime != null && (nextTime.before(end) || nextTime.equals(end))) {
                incomingTasks.add(t);
                System.out.println(t.getTitle());
            }
        }
        return incomingTasks;
    }

    /*
    public SortedMap<Date, Set<Task>> calendar( Date start, Date end){
        Iterable<Task> incomingTasks = incoming(start, end);
        TreeMap<Date, Set<Task>> calendar = new TreeMap<>();

        for (Task t : incomingTasks){
            Date nextTimeAfter = t.nextTimeAfter(start);
            while (nextTimeAfter!= null && (nextTimeAfter.before(end) || nextTimeAfter.equals(end))){
                if (calendar.containsKey(nextTimeAfter)){
                    calendar.get(nextTimeAfter).add(t);
                }
                else {
                    HashSet<Task> oneDateTasks = new HashSet<>();
                    oneDateTasks.add(t);
                    calendar.put(nextTimeAfter,oneDateTasks);
                }
                nextTimeAfter = t.nextTimeAfter(nextTimeAfter);
            }
        }
        return calendar;
    }
    */
    private Date getDateFromFilterField(LocalDate localDate, String time, DateService dateService){
        Date date = dateService.getDateValueFromLocalDate(localDate);
        return dateService.getDateMergedWithTime(time, date);
    }
}
