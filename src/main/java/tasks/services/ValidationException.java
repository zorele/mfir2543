package tasks.services;

public class ValidationException extends RuntimeException {
    /**
     * Constructor implicit
     */
    public ValidationException() {
    }

    /**
     * Constructor
     * @param message
     */
    public ValidationException(String message) {
        super(message);
    }

    /**
     * Constructor
     * @param message
     * @param cause
     */
    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructor
     * @param cause
     */
    public ValidationException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructor
     * @param message
     * @param cause
     * @param enableSuppression
     * @param writableStackTrace
     */
    public ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
